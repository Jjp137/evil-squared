"""Defines the types of level tiles and the size of a tile.

The size of each tile is defined in the TILE_SIZE constant.

The types of tiles are: NORMAL, WALL, PIT, BREAKABLE, BREAKING, LEFT_TURRET,
RIGHT_TURRET, UP_TURRET, DOWN_TURRET, START, GOAL, BOSS_SPAWN

The BREAKING tile should never be found in level files. It is used as the
intermediate stage for a tile that is breakable.

The BOSS_SPAWN tile should only be found in boss levels.

"""

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# These are the various types of level tiles that can appear in a given level.
NORMAL = '.'
WALL = '#'
PIT = '^'
BREAKABLE = '%'
BREAKING = '!'  # Do not use in level definitions

LEFT_TURRET = '4'
RIGHT_TURRET = '6'
UP_TURRET = '8'
DOWN_TURRET = '2'

START = '@'
GOAL = '*'
BOSS_SPAWN = 'B'  # Only use in boss levels

# This is the diameter of each tile.
TILE_SIZE = 25
