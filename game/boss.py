# Put header here

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# This imports the PyGame library.
import pygame

import math

# This import statement brings in other parts of the game's code.
from game import bullet, data

# Constants
BOSS_SIZE = 40

MOVEMENT = 0
SHOOTING = 1

MOVING_UP = 0
MOVING_DOWN = 1

class Boss(object):
    def __init__(self, pos, pattern_data):
        self.pos = list(pos)
        self.patterns = {}
        self.current_pattern = None
        self.target = None

        self.initial_health = None
        self.health = None
        self.alive = True
        self.hurt_timer = 0  # Used for displaying boss-hurt.png

        # TODO: Temporary
        self.image = data.image_load("default", "boss.png")
        self.hurt_image = data.image_load("default", "boss-hurt.png")

        self._parse_data(pattern_data)

    def _parse_data(self, patterns):
        within_pattern = False
        pattern_section = []
        pattern_hp = None

        for line in patterns:
            if "start_health" in line:
                self.initial_health = int(line.split('=')[1])
                self.health = self.initial_health

            elif "when health is" in line:
                within_pattern = True
                pattern_hp = int(line.split(' ')[-1].rstrip(':'))

            elif "end pattern;" in line:
                within_pattern = False
                self.patterns[pattern_hp] = Pattern(pattern_section)
                pattern_section = []

            elif within_pattern:
                pattern_section.append(line.strip())

        self.current_pattern = self.patterns[self.health]

    def hurt(self, damage):
        if not self.alive:
            return

        self.health -= damage
        if self.health in self.patterns:
            self.current_pattern = self.patterns[self.health]
            print("pattern switch!")

        print(self.health)

        if self.health <= 0:
            self.alive = False
        else:
            self.hurt_timer = 2

    def get_pos_rect(self):
        return pygame.Rect(self.pos[0], self.pos[1], BOSS_SIZE, BOSS_SIZE)

    def update_pos(self):
        if not self.alive:
            return

        self.current_pattern.move_boss(self)

        if self.hurt_timer > 0:
            self.hurt_timer -= 1

    def charge_bullets(self):
        if not self.alive:
            return []

        return self.current_pattern.make_bullets(self)

    def draw(self, canvas):
        if not self.alive:
            return

        if self.hurt_timer > 0:
            canvas.blit(self.hurt_image, self.pos)
        else:
            canvas.blit(self.image, self.pos)


class Pattern(object):
    def __init__(self, data):
        self.behaviors = []

        self._parse_behaviors(data)

    def _create_behavior(self, name, arguments):
        if name == "move_constantly":
            return MoveConstantly(**arguments)
        elif name == "straight":
            return StraightBullets(**arguments)
        elif name == "aimed":
            return AimedBullets(**arguments)
        elif name == "spread":
            return SpreadBullets(**arguments)

    def _parse_behaviors(self, data):
        for line in data:
            argument_dict = {}

            split_line = line.split(':')
            behavior_name = split_line[0]
            behavior_args = split_line[1].strip().split(',')

            for arg in behavior_args:
                split_arg = arg.split('=')
                argument_dict[split_arg[0].strip()] = split_arg[1].strip()

            new_behavior = self._create_behavior(behavior_name, argument_dict)
            self.behaviors.append(new_behavior)

    def move_boss(self, boss):
        for behavior in self.behaviors:
            if behavior.category == MOVEMENT:
                behavior.step(boss)

    def make_bullets(self, boss):
        shot_bullets = []

        for behavior in self.behaviors:
            if behavior.category == SHOOTING:
                shot_bullets.extend(behavior.step(boss))

        return shot_bullets


class Behavior(object):
    def __init__(self, **kwargs):
        self.category = None
        print("The __init__() method needs to be overridden.")

    def step(self, boss):
        print("The step() method needs to be overridden.")


class MoveConstantly(Behavior):
    def __init__(self, **kwargs):
        self.category = MOVEMENT
        self.speed = int(kwargs.get("speed", 3))
        self.current_direction = MOVING_DOWN

    def step(self, boss):
        if self.current_direction == MOVING_DOWN:
            boss.pos[1] += self.speed
        elif self.current_direction == MOVING_UP:
            boss.pos[1] -= self.speed

        if boss.pos[1] <= 30:
            self.current_direction = MOVING_DOWN
        elif boss.pos[1] >= 470:
            self.current_direction = MOVING_UP


class StraightBullets(Behavior):
    def __init__(self, **kwargs):
        self.category = SHOOTING

        self.speed = int(kwargs.get("speed", bullet.BULLET_SPEED))
        self.delay = int(kwargs.get("delay", 60))
        self.timer = int(kwargs.get("initial_pause", 0))

    def step(self, boss):
        bullets = []

        if self.timer == 0:
            self.timer = self.delay

            boss_rect = boss.get_pos_rect()

            bullet_x = boss_rect.topleft[0] - bullet.BULLET_SIZE
            bullet_y = boss_rect.topleft[1] + 10  # Offset
            bullet_pos = (bullet_x, bullet_y)
            bullet_vel = (-self.speed, 0)

            bullets.append(bullet.Bullet(bullet_pos, bullet_vel, False))
        else:
            self.timer -= 1

        return bullets


class AimedBullets(Behavior):
    def __init__(self, **kwargs):
        self.category = SHOOTING

        self.speed = int(kwargs.get("speed", bullet.BULLET_SPEED))
        self.delay = int(kwargs.get("delay", 60))
        self.timer = int(kwargs.get("initial_pause", 0))

    def step(self, boss):
        bullets = []

        if self.timer == 0:
            self.timer = self.delay

            target_pos = boss.target.get_pos_rect().center
            boss_rect = boss.get_pos_rect()

            bullet_x = boss_rect.topleft[0] - bullet.BULLET_SIZE
            bullet_y = boss_rect.topleft[1] + 10  # Offset
            bullet_pos = (bullet_x, bullet_y)

            dist_x = target_pos[0] - bullet_x
            dist_y = target_pos[1] - bullet_y
            hyp = math.sqrt(dist_x ** 2 + dist_y ** 2)

            aimed_x = (self.speed) * (dist_x / hyp)
            aimed_y = (self.speed) * (dist_y / hyp)
            bullet_vel = (aimed_x, aimed_y)

            bullets.append(bullet.Bullet(bullet_pos, bullet_vel, False))
        else:
            self.timer -= 1

        return bullets


class SpreadBullets(Behavior):
    def __init__(self, **kwargs):
        self.category = SHOOTING

        self.speed = int(kwargs.get("speed", bullet.BULLET_SPEED))
        self.delay = int(kwargs.get("delay", 60))
        self.timer = int(kwargs.get("initial_pause", 0))

        self.y_range = float(kwargs.get("y_range", 0.5))
        self.shots = int(kwargs.get("shots", 3))

    def step(self, boss):
        bullets = []

        if self.timer == 0:
            self.timer = self.delay

            boss_rect = boss.get_pos_rect()

            bullet_x = boss_rect.topleft[0] - bullet.BULLET_SIZE
            bullet_y = boss_rect.topleft[1] + 10  # Offset
            bullet_pos = (bullet_x, bullet_y)

            y_interval = (self.y_range * 2) / (self.shots - 1)
            vel_y = -self.y_range
            count = 0

            while count < self.shots:
                bullet_vel = (-self.speed, vel_y)
                bullets.append(bullet.Bullet(bullet_pos, bullet_vel, False))

                vel_y += y_interval
                count += 1
        else:
            self.timer -= 1

        return bullets
