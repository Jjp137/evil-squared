# Put header here

# Python 3 compatibility
from __future__ import print_function, unicode_literals, division

# Game module imports
from game import bullet
from game.tiles import (LEFT_TURRET, RIGHT_TURRET, UP_TURRET,
                        DOWN_TURRET, TILE_SIZE)

# Constants
X_OFFSET = 5
Y_OFFSET = 5

class Turret(object):
    def __init__(self, pos, facing, data=None):
        self.pos = pos
        self.facing = facing

        # The properties below are overridden by the level file if they
        # are provided through the data parameter.

        if data is not None:
            params = data.split(":")
            self.initial_delay = int(params[1])
            self.counter = self.initial_delay
            self.delay_list = [int(i) for i in params[2].split(",")]
        else:
            self.initial_delay = 0
            self.counter = 0
            self.delay_list = [60]

        # Tracks the current position in the delay list
        self._delay_index = 0

        self.bullet_spawn_pos = None
        self.bullet_spawn_vel = None
        self._determine_bullet_params()

    def _determine_bullet_params(self):
        if self.facing == LEFT_TURRET:
            bullet_x = self.pos[0] - bullet.BULLET_SIZE
            bullet_y = self.pos[1] + Y_OFFSET
            self.bullet_spawn_pos = (bullet_x, bullet_y)
            self.bullet_spawn_vel = (-bullet.BULLET_SPEED, 0)

        elif self.facing == RIGHT_TURRET:
            bullet_x = self.pos[0] + TILE_SIZE
            bullet_y = self.pos[1] + Y_OFFSET
            self.bullet_spawn_pos = (bullet_x, bullet_y)
            self.bullet_spawn_vel = (bullet.BULLET_SPEED, 0)

        elif self.facing == UP_TURRET:
            bullet_x = self.pos[0] + X_OFFSET
            bullet_y = self.pos[1] - bullet.BULLET_SIZE
            self.bullet_spawn_pos = (bullet_x, bullet_y)
            self.bullet_spawn_vel = (0, -bullet.BULLET_SPEED)

        elif self.facing == DOWN_TURRET:
            bullet_x = self.pos[0] + X_OFFSET
            bullet_y = self.pos[1] + TILE_SIZE
            self.bullet_spawn_pos = (bullet_x, bullet_y)
            self.bullet_spawn_vel = (0, bullet.BULLET_SPEED)

    def charge_bullet(self):
        self.counter -= 1

        if self.counter <= 0:
            self.counter = self.delay_list[self._delay_index]
            self._delay_index = (self._delay_index + 1) % len(self.delay_list)
        else:
            return None

        return bullet.Bullet(self.bullet_spawn_pos,
                             self.bullet_spawn_vel, False)
