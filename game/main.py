"""TODO: put in an actual docstring"""

# Python 3 compatibility
from __future__ import print_function, unicode_literals, division

# Standard library imports
import sys

# PyGame-specific imports
import pygame
from pygame.locals import *

# Game module imports
from game import data, level, statusbar

# Constants
FPS = 60
FRAME_DELTA = (1 / FPS) * 1000  # In milliseconds

def exit_game():
    pygame.quit()
    sys.exit()

def start_game():
    pygame.init()

    clock = pygame.time.Clock()

    window_size = (800, 600)
    window = pygame.display.set_mode(window_size)

    pygame.display.set_caption("Evil Squared (Alpha)")

    level_num = 6
    level_max = 6  # Temporary

    while True:
        level_won = level_loop(level_num, window, clock)

        if level_won:
            level_num = level_num + 1 if level_num != level_max else 1

def level_loop(level_num, window, clock):
    field = level.load_file(data.level_load("default", level_num))
    status = statusbar.StatusBar(field, clock)
    player_won = False

    frame_lag = 0.0

    while True:
        frame_lag += clock.tick_busy_loop()

        for event in pygame.event.get():
            if event.type == QUIT:
                exit_game()

            elif event.type == MOUSEBUTTONDOWN:
                print(event.pos[0] // 25, event.pos[1] // 25)

            elif (event.type == KEYUP and event.key == K_RETURN
                    and not field.is_active()):
                return player_won

            else:
                field.read_input(event)

        while frame_lag >= FRAME_DELTA:
            field.update()

            if field.player_won():
                player_won = True

            frame_lag -= FRAME_DELTA

        field.draw(window)
        status.draw(window)

        pygame.display.update()
        pygame.time.delay(int(FRAME_DELTA - frame_lag))
