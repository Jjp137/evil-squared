"""The colors module defines common colors as RGB tuples."""

# Python 3 compatibility
from __future__ import print_function, unicode_literals, division

# Colors
WHITE = (255, 255, 255)
GRAY = (128, 128, 128)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
SKYBLUE = (135, 206, 255)
ORANGE = (255, 165, 0)
