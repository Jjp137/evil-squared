"""Contains helper functions for accessing the game's data.

The data.py file in PyWeek's Skellington 1.9 is used as a basis for this
module. Functions have been added that load more specific types of files.

These are the functions within this module:
    - filepath: Obtain a path to a file
    - load: Load a file
    - font_load: Load a font
    - image_load: Load an image
    - level_load: Load a level file

This module does not contain any classes.

"""

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# This imports the PyGame library.
import pygame

# These are imports that refer to modules in the standard library.
import os

# These module variables point to various locations within the data folder's
# hierarchy.
data_py = os.path.abspath(os.path.dirname(__file__))
data_dir = os.path.normpath(os.path.join(data_py, '..', 'data'))

font_dir = os.path.normpath(os.path.join(data_dir, 'fonts'))
image_dir = os.path.normpath(os.path.join(data_dir, 'images'))
level_dir = os.path.normpath(os.path.join(data_dir, 'levels'))
music_dir = os.path.normpath(os.path.join(data_dir, 'music'))
sounds_dir = os.path.normpath(os.path.join(data_dir, 'sounds'))

def filepath(filename):
    """Determine the path to a file in the data directory.

    In practice, this function is not really used since there are
    other functions that work better with the data folder's hierarchy.

    Args:
        filename (str): The name of the file.

    Returns:
        The file path.

    """
    return os.path.join(data_dir, filename)

def load(filename, mode='rb'):
    """Open a file in the data directory.

    In practice, this function is not really used since there are
    other functions that work better with the data folder's hierarchy.

    Args:
        filename (str): The name of the file.
        mode (str): Optional. How the file should be opened. By default,
            it is opened as a read-only, binary file.

    Returns:
        The opened file.

    """
    return open(os.path.join(data_dir, filename), mode)

def font_load(filename, size):
    """Load a font file in the data/fonts directory.

    Args:
        filename (str): The name of the font file.
        size (int): The size (in pt) to render the font.

    Returns:
        A Font object containing the font at the given size.

    """
    font_path = os.path.join(font_dir, filename)

    return pygame.font.Font(font_path, size)

def image_load(folder, filename, alpha=False):
    """Load an image file.

    The image file to be loaded should be located within a
    texture pack directory. The directory itself should be located
    within the data/images directory.

    Args:
        folder (str): The name of the texture pack. This will be the
            directory that will be opened to find the image.
        filename (str):  The image file to load.
        alpha (bool): Optional. Whether the image file is loaded
            with alpha transparency. Defaults to False.

    Returns:
        A Surface object containing the loaded image.

    """
    image_path = os.path.join(image_dir, folder, filename)

    if alpha:
        return pygame.image.load(image_path).convert_alpha()
    else:
        return pygame.image.load(image_path).convert()

def level_load(folder, level_num):
    """Load a level file.

    The level file to be loaded should be located within a level set
    directory. The directory itself should be located within the
    data/levels directory.

    Args:
        folder (str): The name of the folder where the level set is located.
        level_num (int): The level number.

    Returns:
        A file object containing level data.

    """
    filename = "level-" + str(level_num) + ".txt"

    return load(os.path.join(level_dir, folder, filename), 'r')
