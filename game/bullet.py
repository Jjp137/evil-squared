"""Contains logic for bullet behavior.

Two constants, BULLET_SIZE and BULLET_SPEED, and the Bullet class are defined
within this module. This module does not contain any functions.

"""

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# This imports the PyGame library.
import pygame

# This import statement brings in other parts of the game's code.
from game import data

# These are constants that define the bullet's size and default speed.
BULLET_SIZE = 15
BULLET_SPEED = 3

# TODO: remove eventually
cached_enemy = None
cached_friend = None

class Bullet(object):
    """Represents a bullet that has been fired.

    A Bullet object can be created by the player, the boss, or a turret.

    Attributes:
        pos (x, y): The current position of the bullet's top-left corner.
        vel (x, y): The current velocity of the bullet.
        friendly (bool): Whether the bullet was shot by the player. It is
            False if it was shot by something else.

    """

    def __init__(self, pos, vel, friendly):
        """Create a Bullet object.

        Args:
            pos (x, y): The initial position of the bullet's top-left corner.
            vel (x, y): The initial velocity of the bullet.
            friendly (bool): Whether the bullet was shot by the player. It is
                False if it was shot by something else.

        """

        self.pos = list(pos)
        self.vel = list(vel)
        self.friendly = friendly

    def get_pos_rect(self):
        """Get a Rect based on the bullet's current location.

        The Rect can be used for drawing or collision detection.

        Returns:
            A Rect object representing the bullet.

        """

        return pygame.Rect(self.pos[0], self.pos[1], BULLET_SIZE, BULLET_SIZE)

    def update_pos(self):
        """Update the bullet's position based on its current velocity."""

        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]

    def draw(self, canvas):
        """Draw the bullet on a Surface object.

        Args:
            canvas (Surface): The Surface to be drawn on.

        """

        global cached_enemy, cached_friend

        if cached_enemy is None:
            cached_enemy = data.image_load("default", "enemy-bullet.png")
        if cached_friend is None:
            cached_friend = data.image_load("default", "player-bullet.png")

        image = cached_friend if self.friendly else cached_enemy

        canvas.blit(image, self.pos)
