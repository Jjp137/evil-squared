# Put header here

# Python 3 compatibility
from __future__ import print_function, unicode_literals, division

# PyGame-specific imports
import pygame
from pygame.locals import *

# Game module imports
from game import data, bullet

# Player-specific constants
NORMAL_SPEED = 2.5
RUN_SPEED = 4
PLAYER_SIZE = 20
HEALTH_MAX = 3
SHOOT_DELAY = 10  # In frames, so 1/6th of a second

class Player(object):
    def __init__(self, start_pos):
        self.old_pos = list(start_pos)
        self.pos = list(start_pos)

        self.left = False
        self.right = False
        self.up = False
        self.down = False

        self.running = False

        self.can_shoot = False
        self.shoot_cooldown = 0
        self.shooting = False

        self.health = HEALTH_MAX
        self.alive = True
        self.invul_timer = 0

        # TODO: Temporary
        self.image = data.image_load("default", "player.png")

    def get_pos_rect(self):
        return pygame.Rect(self.pos[0], self.pos[1], PLAYER_SIZE, PLAYER_SIZE)

    def hurt(self, damage, bypass_invul=False):
        if not self.alive:
            return

        if self.invul_timer > 0 and not bypass_invul:
            return

        self.health -= damage

        if self.health <= 0:
            self.freeze()
            self.alive = False
        else:
            self.invul_timer = 30  # Half a second

    def freeze(self):
        self.left = False
        self.right = False
        self.up = False
        self.down = False

        self.running = False
        self.shooting = False

    def read_input(self, event):
        if not self.alive:
            return

        if event.type not in (KEYUP, KEYDOWN):
            return

        if event.key == K_LEFT:
            self.left = True if event.type == KEYDOWN else False
        elif event.key == K_RIGHT:
            self.right = True if event.type == KEYDOWN else False
        elif event.key == K_UP:
            self.up = True if event.type == KEYDOWN else False
        elif event.key == K_DOWN:
            self.down = True if event.type == KEYDOWN else False

        elif event.key == K_LSHIFT or event.key == K_RSHIFT:
            self.running = True if event.type == KEYDOWN else False

        elif event.key == K_SPACE and self.can_shoot:
            self.shooting = True if event.type == KEYDOWN else False

    def update(self):
        speed = RUN_SPEED if self.running else NORMAL_SPEED

        self.old_pos = self.pos[:]

        if self.left:
            self.pos[0] -= speed
        if self.right:
            self.pos[0] += speed

        if self.up:
            self.pos[1] -= speed
        if self.down:
            self.pos[1] += speed

        if self.invul_timer > 0:
            self.invul_timer -= 1

    def charge_bullet(self):
        if not self.can_shoot:
            return

        self.shoot_cooldown -= 1

        if self.shoot_cooldown <= 0 and self.shooting:
            player_rect = self.get_pos_rect()

            bullet_x = player_rect.topright[0]
            bullet_y = player_rect.topright[1] + 2
            bullet_pos = (bullet_x, bullet_y)
            bullet_vel = (bullet.BULLET_SPEED * 2, 0)

            self.shoot_cooldown = SHOOT_DELAY

            return bullet.Bullet(bullet_pos, bullet_vel, True)
        else:
            return None

    def draw(self, canvas):
        if self.invul_timer > 0 and self.invul_timer % 3 == 0:
            return

        if not self.alive:
            return

        canvas.blit(self.image, self.pos)
