"""Contains logic for levels.

This module contains the following functions that perform conversions
between tile coordinates and world (or pixel) coordinates:
    - get_pixel_int: convert a tile value to a pixel value
    - get_tile_int: convert a pixel value to a tile value
    - get_pixel_coords: convert tile coords to pixel coords
    - get_tile_coords: convert pixel coords to tile coords
    - get_tile_rect: obtain a Rect from tile coords

This module also contains the Level class, which represents a level in
the game.

TODO: should constants be documented here too?

"""

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# This imports the PyGame library.
import pygame

# Modules from the standard library are then imported.
import copy

# This import statement brings in other parts of the game's code.
from game import boss, data, player, turret
from game.tiles import (NORMAL, WALL, PIT, BREAKABLE, BREAKING,
                        LEFT_TURRET, RIGHT_TURRET, UP_TURRET, DOWN_TURRET,
                        START, GOAL, BOSS_SPAWN, TILE_SIZE)

# These constants represent the two types of levels.
MAZE = "maze"
BOSS = "boss"

# These constants represent the possible states that a level can be in.
ACTIVE = 0
PLAYER_DEAD = 1
TIME_OVER = 2
BOSS_DEAD = 3  # Only in boss levels
CLEARED = 4
PERFECT_CLEAR = 5

# These constants represent the size of the level.
COLUMNS = 32
ROWS = 22

# These constants represent the markers that separate a level file's sections.
TILE_MARKER = "[Tiles]"
TILE_END_MARKER = "[/Tiles]"
INFO_MARKER = "[Info]"
INFO_END_MARKER = "[/Info]"
TURRET_MARKER = "[Turrets]"
TURRET_END_MARKER = "[/Turrets]"
BOSS_MARKER = "[Boss]"
BOSS_END_MARKER = "[/Boss]"

# This dictionary maps tile constants to their image filenames.
tile_mappings = {START : "floor-tile.png",
                 GOAL : "goal-tile.png",
                 BOSS_SPAWN : "floor-tile.png",
                 NORMAL : "floor-tile.png",
                 WALL : "wall-tile.png",
                 PIT : "pit-tile.png",
                 BREAKABLE : "unstable-tile.png",
                 BREAKING : "breaking-tile.png",
                 LEFT_TURRET : "left-turret.png",
                 RIGHT_TURRET : "right-turret.png",
                 UP_TURRET : "up-turret.png",
                 DOWN_TURRET : "down-turret.png"}
# TODO: replace this later
cached = {}

class Level(object):
    """Represents a level in the game.

    Attributes:
        tiles (list): A two-dimensional ordered list of all the tiles in
            the level. The first index is the x coordinate, and the second
            index is the y coordinate. Tile coordinates are used.
        breaking_tiles (x, y) -> (int): A dictionary that keeps track of
            breakable tiles that are about to turn into pits.

        turrets (list): Contains all the turrets in the level.
        bullets (list): Contains all the bullets currently in the level.

        level_status: Specifies the current state of the level (whether it
            has been completed or if the player ran out of time).
        floor_num (int): The level number.
        time_left (int): The amount of time left to complete the level,
            in frames.

        player (Player): The player that runs around the level.
        goal_rect (Rect): The Rect representing the goal tile. Used for
            collision detection.
        boss (Boss): The boss in the level, if any. This is None if the
            level is not a boss level.

    """

    def __init__(self, tiles, info, **kwargs):
        """Create a Level object.

        Formal args:
            tiles (list): A two-dimension ordered list of all the tiles in the
                level. The first index must be the x coordinate, and the
                second index must be the y coordinate.
            info (dict): A dictionary containing settings from the
                Info section of the level file.

        Keyword args:
            turrets (list): A list of Turret objects.
            boss (dict): A dictionary of health values and bullet patterns
                that the boss will use at each threshold.

        """

        # Initialize all the variables. Note that a deep copy is done for
        # the arrays in order to avoid any horrible aliasing problems.

        self.tiles = copy.deepcopy(tiles)
        self.breaking_tiles = {}
        self.bullets = []

        if "turrets" in kwargs:
            self.turrets = copy.deepcopy(kwargs["turrets"])
        else:
            self.turrets = []

        self.level_status = ACTIVE
        self.floor_num = int(info["floor"])
        self.time_left = int(info["time"]) * 60  # This is in frames

        self.player = None
        self.goal_rect = None
        self.boss = None

        # The start and goal has yet to be defined, so find where they are.

        self._find_start_and_end()

        # Add the boss to the level if this is a boss level.

        if "boss" in kwargs:
            self.player.can_shoot = True  # False by default
            self._spawn_boss(kwargs["boss"])

    def _find_start_and_end(self):
        """Finds the start point and goal of the level.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        for tile_x, column in enumerate(self.tiles):
            for tile_y, tile_type in enumerate(column):
                if tile_type == START:
                    start_pos = get_pixel_pos(tile_x, tile_y)
                    self.player = player.Player(start_pos)

                elif tile_type == GOAL:
                    goal_pos = get_pixel_pos(tile_x, tile_y)
                    self.goal_rect = pygame.Rect(goal_pos[0], goal_pos[1],
                                                 TILE_SIZE, TILE_SIZE)

    def _spawn_boss(self, boss_data):
        """Create the boss if the level is a boss level.

        This is an internal method, and it should not be called from
        outside the Level class.

        Args:
            boss_data (dict): A dictionary of health values and bullet patterns
                that the boss will use at each threshold.

        """

        for tile_x, column in enumerate(self.tiles):
            for tile_y, tile_type in enumerate(column):
                if tile_type == BOSS_SPAWN:
                    boss_pos = get_pixel_pos(tile_x, tile_y)
                    self.boss = boss.Boss(boss_pos, boss_data)
                    self.boss.target = self.player

        if self.boss is None:
            print("Warning: boss spawn missing.")

    def has_boss(self):
        """Returns True if the level is a boss level, and False otherwise."""

        return self.boss is not None

    def is_active(self):
        """Returns True if the level is being played, and False otherwise."""

        # BOSS_DEAD is when the player has just beaten the boss but they
        # have not reached the goal yet.

        return self.level_status in (ACTIVE, BOSS_DEAD)

    def player_lost(self):
        """Returns True if the player lost, and False otherwise."""

        return self.level_status in (PLAYER_DEAD, TIME_OVER)

    def player_won(self):
        """Returns True if the player won, and False otherwise."""

        return self.level_status in (CLEARED, PERFECT_CLEAR)

    def read_input(self, event):
        """Read any user input. Call this method before calling update().

        Args:
            event (Event): A pygame.Event object generated by user input.

        """

        if self.is_active():
            self.player.read_input(event)

    def update(self):
        """Update the level's state by one frame."""

        # Move the player and then adjust its position if it is stuck
        # inside a wall.

        self.player.update()
        self._resolve_tile_collision()

        # Additionally, handle the boss's movement if it's present.

        if self.has_boss():
            self.boss.update_pos()

        # The state of breakable tiles needs to be updated first because
        # they can turn into pits. Only after do we check if the player
        # should die due to falling into a pit.

        self._update_breakables()
        self._handle_pits()

        # Update the bullets afterward.

        self._update_bullets()
        self._handle_bullet_collision()

        # If this is a boss level and it is dead, allow the player to
        # reach the goal.

        if (self.has_boss() and not self.boss.alive and
                self.level_status == ACTIVE):
            self._boss_death_event()
            self.level_status = BOSS_DEAD

        # Exit the method early if the level ended in some way.

        if not self.is_active():
            return

        # Decrement the time if the player is still alive.

        if self.player.alive:

            # Do not subtract time if it is a boss level and the boss is dead.

            if self.level_status != BOSS_DEAD:
                self.time_left -= 1

            # The player can still win if there are exactly zero seconds left,
            # so check if the player reached the goal first.

            if self._check_goal_collision():
                self.player.freeze()

                if self.player.health == player.HEALTH_MAX:
                    self.level_status = PERFECT_CLEAR
                else:
                    self.level_status = CLEARED

            elif self.time_left <= 0:
                self.player.hurt(127, True)
                self.level_status = TIME_OVER

        elif not self.player.alive:
            self.level_status = PLAYER_DEAD

    def _find_colliding_tiles(self, rect, filter_types=None):
        """Retrieve any tiles that collide with the given Rect.

        This is an internal method, and it should not be called from
        outside the Level class.

        Args:
            rect (Rect): The Rect object to check against. The size of the
                Rect should be smaller than a level tile.
            filter_types (list): Optional. A list of tile types that should
                be checked for. If not provided, all tile types are included.

        Returns:
            A set of 4-tuples that represent the tiles that collide with
            the given Rect. These 4-tuples can be used to construct Rect
            objects.

        """

        # Assume that the provided Rect can collide with a maximum of four
        # tiles at any given time. Obtain the tile coordinate that each
        # edge of the Rect is touching.

        tile_left = get_tile_int(rect.left)
        tile_right = get_tile_int(rect.right)
        tile_top = get_tile_int(rect.top)
        tile_bottom = get_tile_int(rect.bottom)

        # Based on the values obtained above, create a list of tile
        # coordinates that correspond to the tiles colliding with the Rect.
        # Also a set is used here. Sets do not allow duplicate elements, which
        # is useful because if the Rect only collides with one tile, the same
        # tile will be represented four times if a set is not used.

        tile_set = set()
        tile_coords = [(tile_left, tile_top), (tile_right, tile_top),
                       (tile_left, tile_bottom), (tile_right, tile_bottom)]

        # Add each colliding tile to the set initialized earlier.
        # Technically, the tile is represented as a 4-tuple, not a Rect
        # object. This is done because Rects are not hashable, and
        # sets require all objects in them to be hashable. 4-tuples can be
        # used to create Rect objects later on, if needed.

        for coords in tile_coords:
            tile_type = self.tiles[coords[0]][coords[1]]

            if filter_types is None or tile_type in filter_types:
                tile_rect = get_tile_rect(coords[0], coords[1])

                if rect.colliderect(tile_rect):
                    tile_set.add(tile_rect)

        return tile_set

    def _inside_walls(self, rect):
        """Check if a given Rect is inside a wall.

        This is an internal method, and it should not be called from
        outside the Level class.

        Args:
            rect (Rect): The Rect to check against. The size of the
                Rect should be smaller than a level tile.

        Returns:
            A boolean indicating whether the Rect is inside a wall.

        """

        # Find all the tiles that are touching the given Rect.
        # Turret tiles are included since nothing should go through them.

        collidables = (WALL, LEFT_TURRET, RIGHT_TURRET, UP_TURRET, DOWN_TURRET)
        walls = self._find_colliding_tiles(rect, collidables)

        # If any walls collide with the given Rect, return True.

        return len(walls) > 0

    def _resolve_tile_collision(self):
        """Adjust the player's position if they are in a wall.

        This method should be called after the player's position is updated.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        # Save some typing.

        player_size = player.PLAYER_SIZE

        # Obtain the current and former positions of the player.

        new_x = self.player.pos[0]
        new_y = self.player.pos[1]
        old_x = self.player.old_pos[0]
        old_y = self.player.old_pos[1]

        # Based on those positions, construct three Rects. The first two
        # Rects treat the movement on each axis separately. The third Rect
        # is based on the current position of the player.

        x_rect = pygame.Rect(new_x, old_y, player_size, player_size)
        y_rect = pygame.Rect(old_x, new_y, player_size, player_size)
        both_rect = pygame.Rect(new_x, new_y, player_size, player_size)

        x_adjusted = False
        y_adjusted = False

        # Move the player on the x-axis only and check if there are any
        # collisions on that axis only. If so, adjust until there is no
        # collision.

        while self._inside_walls(x_rect):
            x_adjusted = True
            if new_x > old_x:
                new_x -= 1
            elif new_x < old_x:
                new_x += 1

            x_rect.x = new_x

        # Do the same with the y-axis.

        while self._inside_walls(y_rect):
            y_adjusted = True
            if new_y > old_y:
                new_y -= 1
            elif new_y < old_y:
                new_y += 1

            y_rect.y = new_y

        # If no adjustments have been made when the axes have been treated
        # seperately, it is possible that when both axes are considered
        # together, a collision exists. Thus, we need to check for that case.

        if not x_adjusted and not y_adjusted:
            while self._inside_walls(both_rect):
                if new_x > old_x:
                    new_x -= 1
                elif new_x < old_x:
                    new_x += 1

                both_rect.x = new_x

                if new_y > old_y:
                    new_y -= 1
                elif new_y < old_y:
                    new_y += 1

                both_rect.y = new_y

        # After adjustments have been made, finalize the player's position.

        self.player.pos = [new_x, new_y]

    def _update_breakables(self):
        """Update the state of all breakable and breaking tiles.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        # Find all the breakable tiles that the player can potentially
        # collide with.

        player_rect = self.player.get_pos_rect()
        breakables = self._find_colliding_tiles(player_rect, (BREAKABLE,))

        # If any breakable tiles collide with the player, start their timers.

        for tile in breakables:  # Skipped if len(breakables) == 0
            tile_x = get_tile_int(tile[0])
            tile_y = get_tile_int(tile[1])
            tile_coords = (tile_x, tile_y)

            self.tiles[tile_x][tile_y] = BREAKING
            self.breaking_tiles[tile_coords] = 60  # One second

        # Decrement the timer for any tiles that are already breaking, and
        # turn them into pits if their timers expired.

        for key in self.breaking_tiles.keys():
            self.breaking_tiles[key] -= 1

            if self.breaking_tiles[key] <= 0:
                self.tiles[key[0]][key[1]] = PIT
                del self.breaking_tiles[key]

    def _handle_pits(self):
        """Check if the player is over a pit, and if so, damage the player.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        # Obtain the list of pit tiles that the player is touching.

        player_rect = self.player.get_pos_rect()
        pits = self._find_colliding_tiles(player_rect, (PIT,))

        # Figure out if too much of the player's area is over a pit. If so,
        # damage the player.

        total_overlap = 0

        for pit in pits:  # Skipped if len(pits) == 0
            pit_rect = pygame.Rect(pit)
            total_overlap += self._rect_intersect_area(player_rect, pit_rect)

        if total_overlap > 200:  # More than 50% of the player's area
            self.player.hurt(127, True)

    def _rect_intersect_area(self, first, second):
        """Calculate the area of intersection for two rectangles.

        This is an internal method, and it should not be called from
        outside the Level class.

        Args:
            first (Rect): The first rectangle.
            second (Rect): The second rectangle.

        Returns:
            The area, in pixels, of the intersection of the two rectangles.

        """

        left = max(first.left, second.left)
        right = min(first.right, second.right)

        # The y-axis is flipped, so flip the max and min around.
        top = max(first.top, second.top)
        bottom = min(first.bottom, second.bottom)

        if left < right and top < bottom:
            return (right - left) * (bottom - top)
        else:
            return 0

    def _update_bullets(self):
        """Update the state of all the bullets in the level.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        # We cannot delete an object from a list while iterating over it, so
        # another list stores all the bullets that we want to delete.

        bullets_to_del = []

        # Move every bullet and delete those that end up in walls.

        for bullet in self.bullets:
            bullet.update_pos()

            bullet_rect = bullet.get_pos_rect()
            if self._inside_walls(bullet_rect):
                bullets_to_del.append(bullet)

        for bullet in bullets_to_del:
            self.bullets.remove(bullet)

        # Keep track of any bullets that the turrets may have fired during
        # this frame.

        for level_turret in self.turrets:
            new_bullet = level_turret.charge_bullet()

            if new_bullet is not None:
                self.bullets.append(new_bullet)

        # Also, take care of the player's bullets.

        player_bullet = self.player.charge_bullet()
        if player_bullet is not None:
            self.bullets.append(player_bullet)

        # Finally, take care of the boss's bullets.

        if self.has_boss():
            boss_bullets = self.boss.charge_bullets()  # Returns a list
            self.bullets.extend(boss_bullets)  # Append the whole list

    def _handle_bullet_collision(self):
        """Handle anything that results from bullet collisions. This includes
        damaging the player and boss.

        This is an internal method, and it should not be called from
        outside the Level class.

        """

        bullets_to_del = []  # Can't delete bullets while iterating
        player_rect = self.player.get_pos_rect()

        if self.has_boss():
            boss_rect = self.boss.get_pos_rect()

        for bullet in self.bullets:
            bullet_rect = bullet.get_pos_rect()

            if (not bullet.friendly and self.player.alive and
                    player_rect.colliderect(bullet_rect)):
                bullets_to_del.append(bullet)
                self.player.hurt(1, False)

            elif (self.has_boss() and bullet.friendly and self.boss.alive and
                    boss_rect.colliderect(bullet_rect)):
                bullets_to_del.append(bullet)
                self.boss.hurt(1)

        for bullet in bullets_to_del:
            self.bullets.remove(bullet)

    def _boss_death_event(self):
        """Perform events that occur when the player beats a boss.

        Specifically, clear all enemy bullets from the screen and change all
        pit tiles to normal tiles.

        """

        # Change all pit tiles to normal tiles.

        for tile_x, column in enumerate(self.tiles):
            for tile_y, tile_type in enumerate(column):
                if tile_type == PIT:
                    column[tile_y] = NORMAL

        # Clear all enemy bullets.

        self.bullets = [bullet for bullet in self.bullets if bullet.friendly]

    def _check_goal_collision(self):
        """Check if the player has collided with the goal.

        This is an internal method, and it should not be called from
        outside the Level class.

        Returns:
            True if the player overlaps the goal, False otherwise.

        """

        player_rect = self.player.get_pos_rect()
        return player_rect.colliderect(self.goal_rect)

    def draw(self, canvas):
        """Draw the level on a Surface object.

        Args:
            canvas (Surface): The Surface to be drawn on.

        """

        # Tiles are drawn before anything else so that the player and bullets
        # are on top of the tiles.

        for tile_x, column in enumerate(self.tiles):
            for tile_y, tile_type in enumerate(column):
                pixel_pos = get_pixel_pos(tile_x, tile_y)

                image_name = tile_mappings[tile_type]

                if tile_type not in cached.keys():
                    cached[tile_type] = data.image_load("default", image_name)

                canvas.blit(cached[tile_type], pixel_pos)

        self.player.draw(canvas)

        if self.has_boss():
            self.boss.draw(canvas)

        for bullet in self.bullets:
            bullet.draw(canvas)


def get_pixel_int(tile_int):
    """Convert an integer in tile coordinates to pixel coordinates.

    Args:
        tile_int (int): An integer in tile coordinates.

    Returns:
        The equivalent integer in pixel coordinates.

    """

    return tile_int * TILE_SIZE

def get_tile_int(pixel_int):
    """Convert an integer in pixel coordinates to tile coordinates.

    Args:
        pixel_int (int): An integer in pixel coordinates.

    Returns:
        The equivalent integer in tile coordinates.

    """

    return pixel_int // TILE_SIZE

def get_pixel_pos(tile_x, tile_y):
    """Convert a set of tile coordinates to pixel coordinates.

    Args:
        tile_x (int): The tile's x-coordinate.
        tile_y (int): The tile's y-coordinate.

    Returns:
        A tuple containing the equivalent pixel coordinates.

    """

    return (tile_x * TILE_SIZE, tile_y * TILE_SIZE)

def get_tile_pos(pixel_x, pixel_y):
    """Convert a set of pixel coordinates to tile coordinates.

    Args:
        pixel_x (int): The x-coordinate, in pixels.
        pixel_y (int): The y-coordinate, in pixels.

    Returns:
        A tuple containing the equivalent tile coordinates.

    """

    return (pixel_x // TILE_SIZE, pixel_y // TILE_SIZE)

def get_tile_rect(tile_x, tile_y):
    """Return the 4-tuple that represents a given tile's Rect.

    Args:
        tile_x (int): The tile's x-coordinate.
        tile_y (int): The tile's y-coordinate.

    Returns:
        A 4-tuple that represents the tile at those coordinates. This 4-tuple
        can later be used to create a Rect.

    """

    pixel_x = get_pixel_int(tile_x)
    pixel_y = get_pixel_int(tile_y)

    return (pixel_x, pixel_y, TILE_SIZE, TILE_SIZE)

def load_file(level_file):
    """Parse a level file and create a Level object out of it.

    Args:
        level_file (file): A file containing level data.

    Returns:
        The Level object created from the given file.

    """

    # Convert the file to a list of lines without newline characters.

    lines = [line.strip() for line in list(level_file)]

    # Break the level file up into three sections, and parse each section
    # of the file. The +1 avoids including the start tag in the data.

    tiles_start = lines.index(TILE_MARKER) + 1
    tiles_end = lines.index(TILE_END_MARKER)
    tiles = _rotate_tiles(lines[tiles_start:tiles_end])

    info_start = lines.index(INFO_MARKER) + 1
    info_end = lines.index(INFO_END_MARKER)
    info_dict = _get_info_dict(lines[info_start:info_end])

    # The third section describes turrets if the level is a non-boss level,
    # but it describes the boss' behavior if the level is a boss level. Thus,
    # we need to check what type of level it is and act accordingly.

    if info_dict["type"] == MAZE:
        turret_start = lines.index(TURRET_MARKER) + 1
        turret_end = lines.index(TURRET_END_MARKER)
        turret_list = _make_turrets(tiles, lines[turret_start:turret_end])

        return Level(tiles, info_dict, turrets=turret_list)

    elif info_dict["type"] == BOSS:
        boss_start = lines.index(BOSS_MARKER) + 1
        boss_end = lines.index(BOSS_END_MARKER)
        boss_data = lines[boss_start:boss_end]

        return Level(tiles, info_dict, boss=boss_data)

def _rotate_tiles(tile_data):
    """Rotate the tiles so that the first index is the x-coordinate and the
    second index is the y-coordinate.

    This is an internal function, and it should not be called from
    outside the Level module.

    Args:
        tile_data (list): A two-dimensional list of tiles that hasn't been
            rotated yet.

    Returns:
        The rotated two-dimensional list of tiles.

    """

    # Initialize the rotated list of tiles.

    tiles = [[] for i in xrange(COLUMNS)]
    tile_count = 0

    # Add each row of tiles to the level object. This is done in a specific
    # way so that tiles can be indexed by self.tiles[x][y], not
    # self.tiles[y][x], which is one of the problems with the PyWeek
    # version's code.

    for tile_y in xrange(len(tile_data)):
        row = list(tile_data[tile_y])
        tile_count += len(row)

        for tile_x in xrange(len(row)):
            tile_type = row[tile_x]
            column = tiles[tile_x]

            column.append(tile_type)

    # Check if the level has the correct amount of tiles, for sanity's sake.

    if tile_count != ROWS * COLUMNS:
        print("Error: level file is malformed.")

    return tiles

def _get_info_dict(lines):
    """Extract the lines written in the info section of the level file
    and place them into a dictionary.

    This is an internal function, and it should not be called from
    outside the Level module.

    Args:
        lines (list): A list of lines containing two strings separated by
            an equals sign.

    Returns:
        The dictionary containing the extracted options for a particular
        level's info section.

    """

    result = {}

    for item in lines:
        options = item.split("=")
        result[options[0].strip()] = options[1].strip()

    return result

def _make_turrets(tiles, turret_data):
    """Create the Turret objects that a level has.

    This is an internal function, and it should not be called from
    outside the Level module.

    Args:
        tiles (list): A two-dimensional list of tiles, rotated so that the
            x-coordinate is the first index and the y-coordinate is the second.
        turret_data (list): The turret section of the level file, without
            the starting and ending markers.

    Returns:
        A list of Turret objects created from the provided data.

    """

    # Temporarily store turret-related data in a dictionary, using the
    # tile coordinates as the key.

    turret_params = {}

    for line in turret_data:
        tile_pos = [int(i) for i in line.split(":")[0].split(",")]  # Um.
        turret_params[(tile_pos[0], tile_pos[1])] = line

    # Go through each tile and find where the turrets are.

    turrets = []

    for tile_x, column in enumerate(tiles):
        for tile_y, tile_type in enumerate(column):
            if tile_type in (LEFT_TURRET, RIGHT_TURRET, UP_TURRET, DOWN_TURRET):
                turret_pos = get_pixel_pos(tile_x, tile_y)
                tile_pos = (tile_x, tile_y)

                # If options for that turret are defined, use those
                # options. Otherwise, set those options to None so that the
                # default parameters are applied to that turret.

                if tile_pos in turret_params:
                    tur_data = turret_params[tile_pos]
                else:
                    tur_data = None

                new_turret = turret.Turret(turret_pos, tile_type, tur_data)
                turrets.append(new_turret)

    return turrets
