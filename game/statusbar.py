"""
TODO: add docstring here
"""

# This import statement is here to aid in future attempts to port to Python 3.
from __future__ import print_function, unicode_literals, division

# This imports the PyGame library.
import pygame

# This import statement brings in other parts of the game's code.
from game import colors, data

class StatusBar(object):
    def __init__(self, level, clock):
        self.font = data.font_load("GranaPadano.ttf", 20)
        self.level = level
        self.fps_clock = clock

        self.flashing_counter = 0
        self.draw_last_bar = True

    def _get_message_surf(self, message, color):
        msg_surf = self.font.render(message, True, color)
        msg_rect = msg_surf.get_rect()
        return msg_surf, msg_rect

    def _get_time_color(self):
        return colors.RED if self.level.time_left < 60 * 10 else colors.WHITE

    def _draw_player_bars(self, start_pos, canvas):
        health = self.level.player.health
        health_color = colors.ORANGE if health == 1 else colors.SKYBLUE

        self.flashing_counter += 1
        if self.flashing_counter >= 30:
            self.draw_last_bar = not self.draw_last_bar
            self.flashing_counter = 0

        if health <= 0:
            return
        elif health == 1 and not self.draw_last_bar:
            return

        for i in xrange(health):
            x_offset = (i + 1) * 12
            bar_pos = (start_pos[0] + x_offset - 4, start_pos[1] + 6)
            bar_rect = pygame.Rect(bar_pos[0], bar_pos[1], 8, 15)
            pygame.draw.rect(canvas, health_color, bar_rect)

    def _draw_boss_bar(self, start_pos, canvas):
        max_health = self.level.boss.initial_health
        cur_health = self.level.boss.health
        bar_length = int(((cur_health * 1.0) / max_health) * 100)

        bar_pos = (start_pos[0] + 10, start_pos[1] + 6)
        bar_rect = pygame.Rect(bar_pos[0], bar_pos[1], bar_length, 15)
        pygame.draw.rect(canvas, colors.RED, bar_rect)

    def draw(self, canvas):
        pygame.draw.line(canvas, colors.GRAY, (0, 550), (800, 550), 5)
        pygame.draw.rect(canvas, colors.BLACK, (0, 550, 800, 50))

        fps = round(self.fps_clock.get_fps())

        fps_msg = "FPS: " + str(fps) #+ " (" + str(time) + "ms)"
        fps_surf, fps_rect = self._get_message_surf(fps_msg, colors.WHITE)
        fps_rect.bottomright = (800, 600)
        canvas.blit(fps_surf, fps_rect)

        fl_msg = "Floor " + str(self.level.floor_num)
        fl_surf, fl_rect = self._get_message_surf(fl_msg, colors.WHITE)
        fl_rect.topleft = (5, 550)
        canvas.blit(fl_surf, fl_rect)

        time_left = "{0:.2f}".format(self.level.time_left / 60)
        time_msg = "Time left: " + str(time_left)
        time_color = self._get_time_color()
        time_surf, time_rect = self._get_message_surf(time_msg, time_color)
        time_rect.topleft = (fl_rect.left, fl_rect.bottom)
        canvas.blit(time_surf, time_rect)

        hp_msg = "Player:"
        hp_surf, hp_rect = self._get_message_surf(hp_msg, colors.WHITE)
        hp_rect.topleft = (200, time_rect.top)
        canvas.blit(hp_surf, hp_rect)

        self._draw_player_bars(hp_rect.topright, canvas)

        if self.level.has_boss() and self.level.boss.alive:
            boss_msg = "Nemesis:"
            boss_surf, boss_rect = self._get_message_surf(boss_msg, colors.WHITE)
            boss_rect.topleft = (340, hp_rect.top)
            canvas.blit(boss_surf, boss_rect)

            self._draw_boss_bar(boss_rect.topright, canvas)
