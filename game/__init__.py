"""The game module contains the code for Evil Squared itself."""

from game import main

if __name__ == "__main__":
    main.start_game()
