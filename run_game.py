"""This tiny script starts Evil Squared.

Usage: run_game.py

It takes in no command line parameters.

"""

import game

if __name__ == "__main__":
    game.main.start_game()
